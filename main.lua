
local filesystem = love.filesystem

filesystem.setRequirePath("lib/?.lua;lib/?/init.lua;"
		..filesystem.getRequirePath())

local event = love.event
local graphics = love.graphics
local system = love.system
local window = love.window

local argparse = require "argparse"
local kglob = require "kglob"
local klass = require "klass"
local kstringex = require "kstringex"
local kloveui = require "kloveui"

local editor = require "editor"
local types = require "editor.data.types"
local Expander = require "editor.ui.Expander"
local View = require "editor.ui.View"
local Dialog = require "editor.ui.Dialog"
local FileDialog = require "editor.ui.FileDialog"

local desktop, root
local toolbar, liveeditcheck, statusbar
local viewpane, newwidgetpane, widgettreepane, propertypane
local clientpane
local selection, classnamelabel, allwidgetoptions
local project

local function showdialog(dialog)
	local w, h = desktop:size()
	local dw, dh = dialog:minsize()
	dialog:pos((w-dw)/2, (h-dh)/2)
	local closed = dialog.closed
	function dialog.closed(_self, data)
		local function bail(stay, ...)
			if not stay then
				_self.closed = closed
				desktop:removechild(_self)
				desktop[#desktop].enabled = not not _self.modal
			end
			return stay, ...
		end
		return bail(closed(_self, data))
	end
	local committed = dialog.committed
	function dialog.committed(_self, data)
		local function bail(stay, ...)
			if not stay then
				_self.committed = committed
				desktop:removechild(_self)
				desktop[#desktop].enabled = not not _self.modal
			end
			return stay, ...
		end
		return bail(committed(_self, data))
	end
	desktop[#desktop].enabled = not dialog.modal
	desktop:addchild(dialog)
end

local function showfiledialog(title, path, patterns, oncommit)
	patterns = kstringex.split(patterns, "|")
	return showdialog(FileDialog {
		modal = true,
		text = title,
		path = path,
		filter = function(_, filename, isdir)
			if isdir then
				return not filename:match("^.")
			end
			for _, pattern in ipairs(patterns) do
				if kglob.match(filename, pattern) then
					return false
				end
			end
			return true
		end,
		committed = function(...)
			return oncommit and oncommit(...)
		end,
	})
end

local messagebuttontexts = {
	y = "Yes",
	n = "No",
	o = "OK",
	c = "Cancel",
	h = "Help",
}

local function showmessage(title, text, buttons)
	local dialog
	buttons = buttons or "o"
	local bb = {
		mode = "h",
		spacing = 4,
		kloveui.Widget {
			expand = true,
		},
	}
	for c in buttons:gmatch(".") do
		bb[#bb+1] = kloveui.Button {
			text = messagebuttontexts[c],
			minw = 80,
			activated = function()
				dialog:committed(c)
			end,
		}
	end
	bb = kloveui.Box(bb)
	local tb = {
		mode = "v",
	}
	for i, line in ipairs(kstringex.split(text, "\n")) do
		tb[i] = kloveui.Label { text=line }
	end
	tb = kloveui.Box(tb)
	dialog = Dialog {
		modal = true,
		text = title,
		kloveui.Box {
			mode = "v",
			tb,
			bb,
		},
	}
	return showdialog(dialog)
end

local function setstatus(fmt, ...)
	statusbar.text = fmt:format(...)
	root:layout()
end

local updatewidgettreepane

local function updatepropertypane()
	while #propertypane > 0 do
		propertypane:removechild(1)
	end
	local wid = selection
	if not wid then return end
	local props = editor.getwidgetprops(wid)
	local groups = { }
	local function getgroup(name)
		local g = groups[name]
		if not g then
			g = { name=name }
			groups[name] = g
			groups[#groups+1] = g
		end
		return g
	end
	getgroup("Common")
	getgroup("General")
	getgroup("Geometry")
	getgroup("Text")
	getgroup("Layout")
	for _, p in ipairs(props or { }) do
		local g = getgroup(p.group or "General")
		g[#g+1] = p
	end
	for _, g in ipairs(groups) do
		if #g > 0 then
			local gw = propertypane:addchild(Expander {
				text = g.name,
				value = true,
				minw = 184,
				maxw = 184,
			})
			for _, p in ipairs(g) do
				local cls = editor.getpropertyeditor(p.type)
				local e = cls {
					expand = true,
					property = p,
					valuechanged = function(_self)
						local v, err = _self:getvalue()
						if err then
							setstatus("Error setting property: "..err)
							return
						end
						wid[p.name] = v
						wid:size(wid.w, wid.h)
						if p.name == "id" then
							updatewidgettreepane()
						end
						root:layout()
					end,
				}
				--local ok, err =
				assert(e:setvalue(wid[p.name]), p.name)
				gw:addchild(kloveui.Box {
					kloveui.Label {
						text = p.name,
						minw = 80,
						maxw = 80,
					},
					e,
					kloveui.Button {
						text = "X",
						activated = function()
							project.modified = true
							updatetitle()
							wid[p.name] = nil
							e:setvalue(wid[p.name])
							e:valuechanged()
						end,
					},
				})
			end
		end
	end
	propertypane:layout()
end

local function setselection(wid)
	for _, o in ipairs(allwidgetoptions or { }) do
		o.value = wid == o._widget
	end
	classnamelabel.text = wid and wid.__name or ""
	selection = wid
	updatepropertypane()
	root:layout()
end

local function addwidgettotreepane(leaf, wid, level)
	local widdesc = wid.id or wid.__name
	local opt = kloveui.Option {
		text = #wid>0 and "This widget" or widdesc,
		minw = 184,
		maxw = 184,
		_widget = wid,
		activated = function(_self)
			kloveui.Option.activated(_self)
			setselection(_self._widget)
		end,
	}
	if #wid > 0 then
		leaf = leaf:addchild(Expander({
			text = widdesc,
			minw = 184,
			maxw = 184,
			value = true,
		})):addchild(kloveui.Box {
			mode = "v",
			minw = 184,
			maxw = 184,
			margin = { l=8 },
		})
	end
	allwidgetoptions[#allwidgetoptions+1] = leaf:addchild(opt)
	for child in wid:children() do
		addwidgettotreepane(leaf, child, level+1)
	end
end

-- local
function updatewidgettreepane()
	while #widgettreepane > 0 do
		widgettreepane:removechild(1)
	end
	allwidgetoptions = { }
	for child in clientpane:children() do
		addwidgettotreepane(widgettreepane, child, 0)
	end
	root:layout()
end

local function updatetitle()
	window.setTitle((project.modified and "*" or "")
			..(project.filename or "Untitled")
			.." - KLÖVEUI Editor")
end

local function newfile()
	if project and (project.modified) then -- luacheck: ignore
		-- TODO: Ask for confirmation.
	end
	project = { }
	updatewidgettreepane()
	setselection()
	setstatus("New project started.")
	updatetitle()
end

--[[
local function readproject(filename)
	--local f = assert(io.open(filename))
	print("TODO: readproject")
end
--]]

local function openfile()
	print("TODO: openfile")
	if project.modified then -- luacheck: ignore
		-- TODO: Ask for confirmation.
	end
end

local savefileas
local reprtable

local function repr(x, seen, t)
	seen = seen or { }
	t = t or (klass:isinstance(x) and x.__name) or type(x)
	local tt = types[t]
	if tt and tt.repr then
		return tt.repr(x)
	elseif t == "string" then
		return ("%q"):format(x)
	elseif t == "table" then
		return reprtable(x, seen)
	else
		return tostring(x)
	end
end

-- local
function reprtable(t, seen)
	seen = seen or { }
	local r = seen[t]
	if r then return r end
	r = { }
	for k, v in pairs(t) do
		r[#r+1] = ("[%s] = %s"):format(repr(k, seen), repr(v, seen))
	end
	r = "{ "..table.concat(r, ", ").." }"
	seen[t] = r
	return r
end

local function writeproject(f)
	local function write(...)
		return assert(f:write(...))
	end
	local function writewidget(wid, level)
		local ind = ("\t"):rep(level)
		local props = editor.getwidgetprops(wid)
		write(ind, "{\n")
		write(ind, ("\tclassname = %q,\n"):format(wid.__name))
		write(ind, "\tproperties = {\n")
		for _, p in ipairs(props) do
			local v = rawget(wid, p.name)
			if v ~= nil then
				write(ind, ("\t\t%s = %s,\n")
						:format(p.name, repr(v, nil, p.type)))
			end
		end
		write(ind, "\t}, -- properties\n")
		for child in wid:children() do
			writewidget(child, level+1)
		end
		write(ind, "}, -- ", wid.id or wid.__name, "\n")
	end
	write("-- KLÖVEUI project file.\n")
	write("return {\n")
	write("\tformatversion = 1,\n")
	write(("\tsavedate = %q,\n"):format(os.date("%Y-%m-%d %H:%M:%S")))
	write(("\teditorversion = %q,\n"):format("1.0"))
	for child in clientpane:children() do
		writewidget(child, 1)
	end
	write("}\n")
end

local function savefile()
	if not project.filename then
		return savefileas()
	end
	xpcall(function()
		local f = assert(io.open(project.filename, "w"))
		writeproject(f)
		assert(f:close())
		setstatus("Project saved.")
		project.modified = nil
		updatetitle()
	end, function(err)
		showmessage("Error", tostring(err))
	end)
end

-- local
function savefileas()
	local function committed(_, value)
		project.filename = value
		updatetitle()
		return savefile()
	end
	showfiledialog("Save project as",
			project.filename or "_tmp_.klui",
			"*.klui",
			committed)
end

local function togglelivepreview()
	project.modified = true
	updatetitle()
	liveeditcheck.value = not liveeditcheck.value
end

local function generatecode()
	showmessage("Error", "Not implemented yet!")
end

local function quit()
	print("TODO: quit")
	event.quit()
end

local function createui()
	liveeditcheck = kloveui.Check {
		text = "Live edit",
		activated = togglelivepreview,
	}

	toolbar = kloveui.Box {
		mode = "h",
		kloveui.Button {
			text = "New",
			activated = newfile,
		},
		kloveui.Button {
			text = "Open...",
			activated = openfile,
		},
		kloveui.Button {
			text = "Save",
			activated = savefile,
		},
		kloveui.Button {
			text = "Save as...",
			activated = savefileas,
		},
		kloveui.Button {
			text = "Generate...",
			activated = generatecode,
		},
		liveeditcheck,
		kloveui.Widget { expand=true },
		kloveui.Button {
			text = "Re-layout",
			activated = function()
				clientpane:layout()
			end,
		},
		kloveui.Button {
			text = "Save dir",
			activated = function()
				system.openURL(filesystem.getSaveDirectory())
			end,
		},
		kloveui.Button {
			text = "Quit",
			activated = quit,
		},
	}

	newwidgetpane = {
		mode = "v",
		text = "Add widget"
	}

	for wid, i in editor.iterwidgets() do
		newwidgetpane[i] = kloveui.Button {
			text = wid.classname,
			activated = function()
				setselection((selection or clientpane):addchild(wid.class { }))
				updatewidgettreepane()
			end,
		}
	end

	newwidgetpane = Expander(newwidgetpane)

	widgettreepane = kloveui.Box {
		mode = "v",
	}

	propertypane = kloveui.Box {
		expand = true,
		mode = "v",
	}

	local function hittest(self, x, y)
		x, y = x-self.x, y-self.y
		for child in self:children(true) do
			local found, rx, ry = hittest(child, x, y)
			if found then
				return found, rx, ry
			end
		end
		if self:inside(x, y) then
			return self, x, y
		end
		return nil
	end

	clientpane = kloveui.Box {
		mode = "v",
		expand = true,
		hittest = function(_self, x, y)
			if liveeditcheck.value then
				return kloveui.Widget.hittest(_self, x, y)
			end
			local wid = hittest(_self, x, y)
			if wid then
				if wid~=_self then
					_self._hit = wid
				else
					_self._hit = nil
				end
				return _self
			else
				_self._hit = nil
			end
		end,
		mousepressed = function(_self, x, y, b, ...)
			if liveeditcheck.value then
				return kloveui.Box.mousepressed(_self, x, y, b, ...)
			end
			if b == 1 then
				setselection(_self._hit)
				_self._hit = nil
			end
		end,
		paintfg = function(_self, ...)
			if (not liveeditcheck.value) and selection then
				local x, y = _self:abspos()
				local sx, sy = selection:abspos()
				graphics.setColor(0, 0, 0)
				graphics.rectangle("line", sx-x, sy-y,
						selection.w, selection.h)
				graphics.rectangle("line", sx-x+2, sy-y+2,
						selection.w-4, selection.h-4)
				graphics.setColor(255, 255, 255)
				graphics.rectangle("line", sx-x+1, sy-y+1,
						selection.w-2, selection.h-2)
			end
			return kloveui.Box.paintfg(_self, ...)
		end,
		mousemoved = function(...)
			if liveeditcheck.value then
				return kloveui.Box.mousemoved(...)
			end
		end,
		mousereleased = function(...)
			if liveeditcheck.value then
				return kloveui.Box.mousereleased(...)
			end
		end,
		kloveui.Box {
			mode = "v",
			kloveui.Label { text="Label" },
			kloveui.Button { text="Button" },
			kloveui.Check { text="Check 1" },
			kloveui.Check { text="Check 2" },
			kloveui.Option { text="Option 1" },
			kloveui.Option { text="Option 2" },
		},
	}

	statusbar = kloveui.Label {
		text = "",
	}

	classnamelabel = kloveui.Label { text="" }

	viewpane = kloveui.Box {
		mode = "h",
		expand = true,
		kloveui.Box {
			mode = "v",
			View {
				scroll = "v",
				minw = 200,
				maxw = 200,
				expand = true,
				kloveui.Box {
					mode = "v",
					newwidgetpane,
					Expander {
						text = "Widget tree",
						value = true,
						widgettreepane,
					},
				}
			},
			View {
				scroll = "v",
				minw = 200,
				maxw = 200,
				expand = true,
				kloveui.Box {
					mode = "v",
					classnamelabel,
					Expander {
						text = "Properties",
						value = true,
						propertypane,
					},
				},
			},
		},
		clientpane,
	}

	root = kloveui.Box {
		mode = "v",
		toolbar,
		viewpane,
		statusbar,
	}

	desktop = kloveui.Widget {
		layout = function(_self)
			root:rect(0, 0, _self:maxsize())
		end,
		root,
	}
end

local oldkeypressed = kloveui.keypressed
function kloveui.keypressed(key, scan, isrep)
	if #desktop == 1 then
		local meta = kloveui.getshiftstate()
		if key == "n" and meta == "c" then
			newfile()
		elseif key == "o" and meta == "c" then
			openfile()
		elseif key == "s" and meta == "c" then
			savefile()
		elseif key == "s" and meta == "cs" then
			savefileas()
		elseif key == "g" and meta == "c" then
			generatecode()
		elseif key == "p" and meta == "c" then
			togglelivepreview()
		else
			return oldkeypressed(key, scan, isrep)
		end
	else
		return oldkeypressed(key, scan, isrep)
	end
end

function love.load(arg)
	local parser = argparse("kloveui-editor", "KLÖVEUI Editor")

	parser:flag "-g" "--generate"
			:description "Generate Lua source from input."
			:argname "FILE"
			:target "output"

	parser:option "-o" "--output"
			:description "Set output file."
			:args(1)
			:argname "FILE"
			:target "output"

	parser:argument "input"
			:description "Input file."
			:args "?"
			:argname "FILE"
			:target "input"

	local opts = parser:parse(arg)

	opts.w, opts.h = 800, 600

	window.setMode(opts.w, opts.h, {
		resizable = true,
	})

	createui()
	newfile()
	desktop:run()
end
