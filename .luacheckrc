
unused_args = false

exclude_files = {
	"_/",
	"lib/editor/data/",
	"lib/simpleui/",
	"lib/klass/",
	"lib/kglob/",
	"lib/argparse.lua",
}

read_globals = {
	"love",
}

files["lib/editor/main.lua"] = {
	globals = { "love" },
}

files["conf.lua"] = {
	globals = { "love" },
}
