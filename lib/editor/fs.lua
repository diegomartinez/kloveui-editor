
---
-- TODO: Doc.
--
-- @module editor.fs

local fs = { }

---
-- TODO: Doc.
--
-- @function listdir
-- @tparam string path
-- @treturn table

---
-- TODO: Doc.
--
-- @function isdir
-- @tparam string path
-- @treturn boolean

---
-- TODO: Doc.
--
-- @function isfile
-- @tparam string path
-- @treturn boolean

do
	local ok, lfs = pcall(require, "lfs")
	if ok then
		fs.rootdir = love.filesystem.getSaveDirectory()
		function fs.listdir(dir)
			local t, n = { }, 0
			for file in lfs.dir(dir) do
				if file ~= "." then
					n=n+1 t[n] = file
				end
			end
			table.sort(t)
			return t
		end
		function fs.isfile(filename)
			local a = lfs.attributes(filename)
			return a and a.mode == "file" or false
		end
		function fs.isdir(filename)
			local a = lfs.attributes(filename)
			return a and a.mode == "directory" or false
		end
		function fs.getcwd()
			local d = lfs.currentdir()
			if not d:match("/$") then d = d.."/" end
			return d
		end
	else
		io.stderr:write("*** WARNING: LuaFileSystem not found."
				.." Using LOVE functions.\n")
		fs.rootdir = "/"
		fs.listdir = love.filesystem.getDirectoryItems
		fs.isfile = love.filesystem.isFile
		fs.isdir = love.filesystem.isDirectory
		function fs.getcwd()
			return fs.rootdir
		end
	end
end

return fs
