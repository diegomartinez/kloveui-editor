
---
-- TODO: Doc.
--
-- **Extends:** `kloveui.Box`
--
-- @classmod editor.ui.Dialog

local graphics = love.graphics

local Box = require "kloveui.Box"
local Label = require "kloveui.Label"
local Button = require "kloveui.Button"

local Dialog = Box:extend("editor.ui.Dialog")

---
-- TODO: Doc.
--
-- @tfield string text
Dialog.text = ""

Dialog.mode = "v"
Dialog.padding = 4
Dialog.spacing = 4

---
-- TODO: Doc.
--
-- @tfield boolean value
Dialog.value = false

local function drawrect(x, y, w, h, bg, bd)
	graphics.setColor(bg)
	graphics.rectangle("fill", x, y, w, h)
	graphics.setColor(bd)
	graphics.rectangle("line", x, y, w, h)
end

function Dialog:init(params)
	Box.init(self, params)
	local dragging
	self._title = Label {
		text = self.text,
		bgcolor = { 0, 0, 128 },
		expand = true,
		paintbg = function(_self)
			Box.paintbg(_self)
			drawrect(0, 0, _self.w, _self.h, _self.bgcolor, _self.bordercolor)
		end,
		mousepressed = function(_self, _, _, b)
			if b == 1 then
				dragging = true
			end
		end,
		mousereleased = function(_self, _, _, b)
			if b == 1 then
				dragging = nil
			end
		end,
		mousemoved = function(_, _, _, dx, dy)
			if dragging then
				self:pos(self.x+dx, self.y+dy)
			end
		end,
	}
	self:addchild(Box {
		mode = "h",
		self._title,
		Button {
			text = "X",
			activated = function()
				self:closed()
			end,
		},
	}, 1)
	self:size(self.w, self.h)
end

function Dialog:paintbg()
	drawrect(0, 0, self.w, self.h, self.bgcolor, self.bordercolor)
end

---
-- TODO: Doc.
--
function Dialog:closed()
end

---
-- TODO: Doc.
--
function Dialog:committed()
end

return Dialog
