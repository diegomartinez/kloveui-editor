
---
-- TODO: Doc.
--
-- **Extends:** `editor.ui.Dialog`
--
-- @classmod editor.ui.ProjectInfoDialog

--~ local gfx = love.graphics

local Dialog = require "editor.ui.Dialog"
-- TODO: local View = require "editor.ui.View"
local Widget = require "kloveui.Widget"
local Box = require "kloveui.Box"
local Label = require "kloveui.Label"
local Entry = require "kloveui.Entry"
local Button = require "kloveui.Button"

local ProjectInfoDialog = Dialog:extend("editor.ui.ProjectInfoDialog")

ProjectInfoDialog.text = "Project Information"
ProjectInfoDialog.minw = 480
ProjectInfoDialog.minh = 360

function ProjectInfoDialog:init(params)
	Dialog.init(self, params)
	local infolist = {
		mode = "v",
		expand = true,
	}
	local function addentry(id, text, value)
		local e = Entry {
			text = value,
			expand = true,
		}
		infolist[#infolist+1] = Box {
			mode = "h",
			Label {
				text = text,
				minw = 96,
			},
			e,
		}
		return e
	end
	infolist = Box(infolist)
	local project = params.project and params.project
	self._commententry = addentry("comment", "Comment",
			project.comment or "")
	self:addchild(Box {
		mode = "v",
		expand = true,
		infolist,
		Box {
			mode = "h",
			Widget {
				expand = true,
			},
			Button {
				text = "OK",
				minw = 80,
				activated = function()
					project.comment = self._commententry.text
					self:committed(true)
				end,
			},
			Button {
				text = "Cancel",
				minw = 80,
				activated = function()
					self:closed()
				end,
			},
		},
	}, 2)
	self:size(self.w, self.h)
end

return ProjectInfoDialog
