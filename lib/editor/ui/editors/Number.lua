
---
-- TODO: Doc.
--
-- **Extends:** `editor.ui.editors.String`
--
-- @classmod editor.ui.editors.Number

local String = require "editor.ui.editors.String"

local Number = String:extend("editor.ui.editors.Number")

function Number:validate(value)
	return value==nil and 0 or tonumber(value), "invalid number"
end

return Number
