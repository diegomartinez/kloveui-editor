
---
-- TODO: Doc.
--
-- **Extends:** `editor.ui.editors.Editor`
--
-- @classmod editor.ui.editors.Border

local Editor = require "editor.ui.editors.Editor"

local Expander = require "editor.ui.Expander"
local Box = require "kloveui.Box"
local Label = require "kloveui.Label"
local Entry = require "kloveui.Entry"

local Border = Editor:extend("editor.ui.editors.Border")

local function entrybox(self, id, label)
	local e = Entry {
		id = id,
		text = "",
		expand = true,
		committed = function()
			return self:valuechanged()
		end,
	}
	self["_"..id] = e
	return Box {
		mode = "h",
		Label { text=label, minw=16 },
		e,
	}
end

function Border:createeditor()
	self._exp = Expander {
		text = "(0,0,0,0)",
		entrybox(self, "l", "L"),
		entrybox(self, "t", "T"),
		entrybox(self, "r", "R"),
		entrybox(self, "b", "B"),
	}
	return self._exp
end

function Border:setvalue(value)
	if type(value) == "number" then
		self._l.text = tostring(value)
		self._t.text = tostring(value)
		self._r.text = tostring(value)
		self._b.text = tostring(value)
	elseif type(value) == "table" then
		self._l.text = tostring(value.l or 0)
		self._t.text = tostring(value.t or 0)
		self._r.text = tostring(value.r or 0)
		self._b.text = tostring(value.b or 0)
	else
		return nil, "invalid type"
	end
	self._exp.text = ("(%s,%s,%s,%s)"):format(
			self._l.text, self._t.text, self._r.text, self._b.text)
	return true
end

function Border:getvalue()
	local l = tonumber(self._l.text)
	local t = tonumber(self._t.text)
	local r = tonumber(self._r.text)
	local b = tonumber(self._b.text)
	if not (l and t and r and b) then
		return nil, "one of the borders is not a valid number"
	end
	return { l=l, t=t, r=r, b=b }
end

return Border
