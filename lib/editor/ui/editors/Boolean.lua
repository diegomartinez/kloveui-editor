
---
-- TODO: Doc.
--
-- **Extends:** `editor.ui.editors.Editor`
--
-- @classmod editor.ui.editors.Boolean

local Editor = require "editor.ui.editors.Editor"

local Check = require "kloveui.Check"

local Boolean = Editor:extend("editor.ui.editors.Boolean")

function Boolean:createeditor()
	self._check = Check {
		expand = true,
		activated = function(_self)
			Check.activated(_self)
			_self.text = _self.value and "True" or "False"
			return self:valuechanged()
		end,
	}
	return self._check
end

function Boolean:setvalue(value)
	self._check.text = value and "True" or "False"
	self._check.value = value and true or false
	return true
end

function Boolean:getvalue()
	return self._check.value
end

return Boolean
