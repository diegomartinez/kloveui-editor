
---
-- TODO: Doc.
--
-- **Extends:** `editor.ui.editors.Editor`
--
-- @classmod editor.ui.editors.Color

local Editor = require "editor.ui.editors.Editor"
local Expander = require "editor.ui.Expander"

local Box = require "kloveui.Box"
local Label = require "kloveui.Label"
local Entry = require "kloveui.Entry"
local Slider = require "kloveui.Slider"

local Color = Editor:extend("editor.ui.editors.Color")

local function sliderbox(self, id, label)
	local e = Entry {
		text = "",
		expand = true,
		committed = function()
			return self:valuechanged()
		end,
	}
	local s = Slider {
		text = "",
		expand = true,
		committed = function()
			return self:valuechanged()
		end,
	}
	self["_"..id] = { e=e, s=s }
	return Box {
		mode = "h",
		Label { text=label, minw=16 },
		e,
		s,
	}
end

function Color:createeditor()
	self._exp = Expander {
		text = "(0,0,0,0)",
		sliderbox(self, "r", "R"),
		sliderbox(self, "g", "G"),
		sliderbox(self, "b", "B"),
		sliderbox(self, "a", "A"),
	}
	return self._exp
end

function Color:setvalue(value)
	if type(value) == "table" then
		self._r.s.value = (value[1] or 0)/255
		self._r.e.text = tostring(math.floor(math.floor(self._r.s.value)*255))
		self._g.s.value = (value[2] or 0)/255
		self._g.e.text = tostring(math.floor(math.floor(self._g.s.value)*255))
		self._b.s.value = (value[3] or 0)/255
		self._b.e.text = tostring(math.floor(math.floor(self._b.s.value)*255))
		self._a.s.value = (value[4] or 255)/255
		self._a.e.text = tostring(math.floor(math.floor(self._a.s.value)*255))
	else
		return nil, "invalid type"
	end
	self._exp.text = ("(%s,%s,%s,%s)"):format(
			self._r.e.text, self._g.e.text, self._b.e.text, self._a.e.text)
	return true
end

function Color:getvalue()
	local r = tonumber(self._r.e.text)
	local g = tonumber(self._g.e.text)
	local b = tonumber(self._b.e.text)
	local a = tonumber(self._a.e.text)
	if not (r and g and b and a) then
		return nil, "one of the components is not a valid number"
	end
	return { r, g, b, a }
end

return Color
