
---
-- TODO: Doc.
--
-- **Extends:** `kloveui.Box`
--
-- **Direct subclasses:**
--
-- * `editor.ui.editors.Boolean`
-- * `editor.ui.editors.Border`
-- * `editor.ui.editors.Color`
-- * `editor.ui.editors.String`
--
-- @classmod editor.ui.editors.Editor

local Box = require "kloveui.Box"
local Label = require "kloveui.Label"

local Editor = Box:extend("editor.ui.editors.Editor")

function Editor:init(params)
	Box.init(self, params)
	self:addchild(self:createeditor())
end

---
-- TODO: Doc.
--
function Editor:createeditor()
	return Label { text=tostring(self.value) }
end

---
-- TODO: Doc.
--
-- @treturn any value
function Editor:getvalue()
end

---
-- TODO: Doc.
--
-- @tparam any value
function Editor:setvalue(value)
	return true
end

---
-- TODO: Doc.
--
function Editor:valuechanged()
end

return Editor
