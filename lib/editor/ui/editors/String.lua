
---
-- TODO: Doc.
--
-- **Extends:** `editor.ui.editors.Editor`
--
-- **Direct subclasses:**
--
-- * `editor.ui.editors.Number`
--
-- @classmod editor.ui.editors.String

local Editor = require "editor.ui.editors.Editor"

local Entry = require "kloveui.Entry"

local String = Editor:extend("editor.ui.editors.String")

function String:createeditor()
	self._e = Entry {
		text = "",
		expand = true,
		committed = function()
			return self:valuechanged()
		end,
	}
	return self._e
end

function String:setvalue(value)
	local v, err = self:validate(value)
	if v == nil then return nil, err end
	self._e.text = tostring(value)
	self._e.index = 0
	return true
end

function String:getvalue()
	local value = self._e.text
	local v, err = self:validate(value)
	if v == nil then return nil, err end
	return v
end

---
-- TODO: Doc.
--
-- @tparam any value
function String:validate(value)
	return value==nil and "" or tostring(value)
end

return String
