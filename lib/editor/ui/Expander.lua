
---
-- TODO: Doc.
--
-- **Extends:** `kloveui.Box`
--
-- @classmod editor.ui.Expander

local graphics = love.graphics

local Box = require "kloveui.Box"
local Check = require "kloveui.Check"

local Expander = Box:extend("editor.ui.Expander")

---
-- TODO: Doc.
--
-- @tfield string text
Expander.text = ""

Expander.mode = "v"

---
-- TODO: Doc.
--
-- @tfield boolean value
Expander.value = false

function Expander:init(params)
	Box.init(self, params)
	self._check = self:addchild(Check {
		text = self.text,
		value = self.value,
		activated = function(_self)
			Check.activated(_self)
			self.value = _self.value
			if self.value then
				self:show()
			else
				self:hide()
			end
		end,
		paintcheck = function(_self, value, x, y, size)
			graphics.push()
			size = size/2
			graphics.translate(x+size, y+size)
			if value then
				graphics.rotate(math.pi/2)
			end
			graphics.setColor(_self.enabled
					and _self.fgcolor
					or _self.fgcolordisabled)
			graphics.polygon("fill", -size, -size, size, 0, -size, size)
			graphics.setColor(_self.bordercolor)
			graphics.polygon("line", -size, -size, size, 0, -size, size)
			graphics.pop()
		end,
	}, 1)
	if self.value then
		self:show()
	else
		self:hide()
	end
end

local function layoutroot(self)
	local last
	while self do
		last = self
		self = self.weakrefs.parent
	end
	last:layout()
end

function Expander:paintbg()
	graphics.setColor(1, 1, 1, .025)
	graphics.rectangle("fill", 0, 0, self.w, self.h)
end

function Expander:addchild(wid, pos)
	if (not self._check) or self.value then
		return Box.addchild(self, wid, pos)
	else
		wid.weakrefs.parent = self
		self._hidden = self._hidden or { "dummy" }
		table.insert(self._hidden, pos or #self._hidden+1, wid)
		return wid
	end
end

---
-- TODO: Doc.
--
function Expander:show()
	self.value = true
	self._check.value = true
	if self._hidden then
		for i = 2, #self._hidden do
			self[i] = self._hidden[i]
		end
		self._hidden = nil
		layoutroot(self)
	end
end

---
-- TODO: Doc.
--
function Expander:hide()
	self.value = false
	self._check.value = false
	if not self._hidden then
		self._hidden = { "dummy" }
		for i = 2, #self do
			self._hidden[i] = self[i]
			self[i] = nil
		end
		layoutroot(self)
	end
end

function Expander:valuechanged()
end

return Expander
