
---
-- TODO: Doc.
--
-- **Extends:** `kloveui.Widget`
--
-- @classmod editor.ui.View

local graphics = love.graphics

local Widget = require "kloveui.Widget"
local Slider = require "kloveui.Slider"

local View = Widget:extend("editor.ui.View")

---
-- TODO: Doc.
--
-- @tfield string scroll `""`, `"h"`, `"v"`, `"hv"` or `"vh"`. Default is `""`.
View.scroll = ""

function View:init(params)
	self._viewx, self._viewy = 0, 0
	self._vieww, self._viewh = 0, 0
	self._internal = 0
	Widget.init(self, params)
	if self.scroll:find("h") then
		self._hsb = Slider {
			minw=1,
			anchor="l",
			valuechanged = function(_self)
				self._viewx = _self.value
				self:setview(self._viewx, self._viewy)
			end,
		}
		self:addchild(self._hsb, 1)
		self._internal = self._internal + 1
	end
	if self.scroll:find("v") then
		self._vsb = Slider {
			minh=1,
			anchor="t",
			valuechanged = function(_self)
				self._viewy = _self.value
				self:setview(self._viewx, self._viewy)
			end,
		}
		self:addchild(self._vsb, 1)
		self._internal = self._internal + 1
	end
	self:layout()
end

--[[
local function getcanvas(self)
	if not (self._canvas
			and self._canvasw==self.w
			and self._canvash==self.h) then
		self._canvas = graphics.newCanvas(self.w, self.h)
		self._canvasw, self._canvash = self.w, self.h
	end
	return self._canvas
end
--]]

---
-- TODO: Doc.
--
-- @tparam number x
-- @tparam number y
-- @treturn number x
-- @treturn number y
function View:setview(x, y)
	x, y = x or self._viewx, y or self._viewy
	self._viewx, self._viewy = x, y
	local vx = x*(self._vieww-self.w)
	local vy = y*(self._viewh-self.h)
	for i = self._internal+1, #self do
		local child = self[i]
		child.x, child.y = -vx, -vy
	end
	return x, y
end

function View:calcminsize()
	local sw, sh = 0, 0
	if self._vsb then
		local t = self._vsb:minsize()
		sw = t
	end
	if self._hsb then
		local _, t = self._hsb:minsize()
		sh = t
	end
	local w, h = 1, 1
	for i = self._internal+1, #self do
		local child = self[i]
		local ww, wh = child:minsize()
		w, h = math.max(w, ww), math.max(h, wh)
	end
	return self._hsb and 128 or sw+w, self._vsb and 128 or sh+h
end

function View:layout()
	local vx = self._viewx*(self._vieww-self.w)
	local vy = self._viewy*(self._viewh-self.h)
	local sw, sh = 0, 0
	if self._vsb then
		self._vsb:rect(self.w-16, 0,
				16, self.h-(self._hsb and 16 or 0), 16)
		sw = self._vsb.w
	end
	if self._hsb then
		self._hsb:rect(0, self.h-16,
				self.w-(self._vsb and 16 or 0), 16)
		sh = self._hsb.h
	end
	local w, h = 1, 1
	for i = self._internal+1, #self do
		local child = self[i]
		local _, _, ww, wh = child:rect(-vx, -vy, self.w-sw, self.h-sh)
		w, h = math.max(w, ww), math.max(h, wh)
	end
	self._vieww, self._viewh = w, h
end

function View:hittest(x, y)
	if not self:inside(x-self.x, y-self.y) then return end
	return Widget.hittest(self, x, y)
end

function View:draw()
	local ax, ay = self:abspos()
	local sc = { graphics.getScissor() }
	graphics.setScissor(ax, ay, self.w, self.h)
	Widget.draw(self)
	graphics.setScissor(unpack(sc))
	--[[
	local c = getcanvas(self)
	local cc = graphics.getCanvas()
	graphics.setCanvas(c)
	graphics.push()
	graphics.translate(-ax, -ay)
	Widget.draw(self)
	graphics.pop()
	graphics.setCanvas(cc)
	graphics.push()
	graphics.translate(self.x, self.y)
	graphics.draw(c)
	graphics.pop()
	--]]
end

return View
