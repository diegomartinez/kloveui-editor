
---
-- TODO: Doc.
--
-- **Extends:** `editor.ui.Dialog`
--
-- @classmod editor.ui.FileDialog

local kpath = require "kpath"
local fs = require "editor.fs"

local Box = require "kloveui.Box"
local Entry = require "kloveui.Entry"
local Button = require "kloveui.Button"

local Dialog = require "editor.ui.Dialog"
local View = require "editor.ui.View"

local FileDialog = Dialog:extend("editor.ui.FileDialog")

---
-- TODO: Doc.
--
-- @tfield string path
FileDialog.path = fs.rootdir

local function itercomps(path)
	return coroutine.wrap(function()
		local i = 0
		for part in path:gmatch("[^\\/]+") do
			if part ~= "" then
				i = i + 1
				coroutine.yield(part, i)
			end
		end
	end)
end

function FileDialog:init(params)
	Dialog.init(self, params)
	self._pathentry = Entry {
		text = self.path,
		expand = true,
		committed = function()
			self:committed(self.path..self._pathentry.text)
		end,
	}
	self._pathbox = Box {
		maxw = 480,
		mode = "h",
	}
	self._pathview = View {
		maxw = 480,
		minh = 40,
		scroll = "h",
		self._pathbox,
	}
	self._dirlist = Box {
		--~ maxw = 480-16,
		--~ __debug = true,
		mode = "v",
	}
	self._filelist = Box {
		--~ maxw = 480-16,
		--~ __debug = true,
		mode = "v",
	}
	self:addchild(Box{
		mode = "v",
		self._pathview,
		View {
			--~ __debug = true,
			minw = 480,
			minh = 240,
			maxw = 480,
			maxh = 240,
			scroll = "v",
			expand = true,
			Box {
				mode = "v",
				self._dirlist,
				self._filelist,
			},
		},
		Box {
			mode = "h",
			self._pathentry,
			spacing = 4,
			Button {
				text = "OK",
				minw = 80,
				activated = function()
					self:committed(self.path..self._pathentry.text)
				end,
			},
			Button {
				text = "Cancel",
				minw = 80,
				activated = function()
					self:closed()
				end,
			},
		},
	}, 2)
	self:size(self.w, self.h)
	self:setpath(self.path)
end

---
-- TODO: Doc.
--
-- @tparam string path
-- @tparam ?boolean update
function FileDialog:setpath(path, update)
	local d, f = kpath.dirname(path), kpath.basename(path)
	self.path = d~="" and d or fs.getcwd()
	if f ~= "" then
		self._pathentry.text = f
		self._pathentry.index = 0
	end
	if update ~= false then
		while #self._pathbox > 0 do
			self._pathbox:removechild(1)
		end
		while #self._dirlist > 0 do
			self._dirlist:removechild(1)
		end
		while #self._filelist > 0 do
			self._filelist:removechild(1)
		end
		local full = { }
		local abs = kpath.isabs(self.path) or ""
		for comp, i in itercomps(self.path) do
			full[i] = comp
			local fp = abs..table.concat(full, "/").."/"
			self._pathbox:addchild(Button {
				text = comp,
				activated = function()
					self:setpath(fp)
				end,
			})
		end
		self._pathview.value = 1
		self._pathview:layout()
		if not fs.isdir(self.path) then
			self:layout()
			return nil, "not a directory"
		end
		local files = fs.listdir(self.path)
		table.sort(files)
		for _, file in ipairs(files) do
			local isd = fs.isdir(self.path.."/"..file)
			if self:filter(file, isd) then -- luacheck: ignore
				-- Do nothing.
			elseif isd then
				self._dirlist:addchild(Button {
					text = file.."/",
					texthalign = 0,
					activated = function()
						self:setpath(self.path..file.."/")
					end,
				})
			elseif fs.isfile(self.path.."/"..file) then
				self._filelist:addchild(Button {
					text = file,
					texthalign = 0,
					activated = function()
						self:setpath(self.path..file, false)
					end,
				})
			end
		end
	end
	self:layout()
	return true
end

---
-- TODO: Doc.
--
-- @tparam string filename
-- @tparam boolean dir
-- @treturn boolean remove
function FileDialog:filter(filename, dir)
end

return FileDialog
