
return {
	string = {
		editorclassname = "editor.ui.editors.String",
		repr = function(v)
			return ("%q"):format(v)
		end,
	},
	number = {
		editorclassname = "editor.ui.editors.Number",
		repr = tostring,
	},
	boolean = {
		editorclassname = "editor.ui.editors.Boolean",
		repr = tostring,
	},
	["simpleui.Border"] = {
		editorclassname = "editor.ui.editors.Border",
		repr = function(v)
			return (("{ l=%s, t=%s, r=%s, b=%s }")
					:format(v.l or 0, v.t or 0, v.r or 0, v.b or 0))
		end,
	},
	["simpleui.Color"] = {
		editorclassname = "editor.ui.editors.Color",
		repr = function(v)
			local r, g, b, a = v[1], v[2], v[3], v[4]
			return (("{ %s, %s, %s%s }")
					:format(r, g, b, a and ", "..a or ""))
		end,
	},
}
