return {
	{
		classname = "kloveui.Widget",
		properties = {
			{
				name = "id",
				type = "string",
				group = "Common",
				description = "ID of this widget.",
			},
			{
				name = "x",
				type = "number",
				group = "Geometry",
				description = "Widget's horizontal position relative to its parent's origin.",
			},
			{
				name = "y",
				type = "number",
				group = "Geometry",
				description = "Widget's vertical position relative to its parent's origin.",
			},
			{
				name = "w",
				type = "number",
				group = "Geometry",
				description = "Widget's current width.",
			},
			{
				name = "h",
				type = "number",
				group = "Geometry",
				description = "Widget's current height.",
			},
			{
				name = "minw",
				type = "number",
				group = "Geometry",
				description = "Widget's minimum width.",
			},
			{
				name = "minh",
				type = "number",
				group = "Geometry",
				description = "Widget's minimum height.",
			},
			{
				name = "maxw",
				type = "number",
				group = "Geometry",
				description = "Widget's maximum width.",
			},
			{
				name = "maxh",
				type = "number",
				group = "Geometry",
				description = "Widget's maximum height.",
			},
			{
				name = "enabled",
				type = "boolean",
				description = "Whether or not the widget is enabled."
					.."\n"
					.."\nDisabled widgets don't receive input events.",
			},
			{
				name = "fgcolor",
				type = "kloveui.Color",
				group = "Colors",
				description = "Color for foreground graphics.",
			},
			{
				name = "fgcolordisabled",
				type = "kloveui.Color",
				group = "Colors",
				description = "Color for foreground graphics when disabled.",
			},
			{
				name = "bgcolor",
				type = "kloveui.Color",
				group = "Colors",
				description = "Color for background graphics.",
			},
			{
				name = "bgcolorraised",
				type = "kloveui.Color",
				group = "Colors",
				description = "Color for background graphics when raised.",
			},
			{
				name = "bgcolorsunken",
				type = "kloveui.Color",
				group = "Colors",
				description = "Color for background graphics when sunken.",
			},
			{
				name = "bordercolor",
				type = "kloveui.Color",
				group = "Colors",
				description = "Color for the border.",
			},
			{
				name = "expand",
				type = "boolean",
				group = "Layout",
				description = "Whether the widget should be expanded as much as possible."
					.."\n"
					.."\nMay be used by layout widgets.",
			},
			{
				name = "margin",
				type = "kloveui.Border",
				group = "Layout",
				description = "Margin around the widget."
					.."\n"
					.."\nMay be used by layout widgets.",
			},
			{
				name = "padding",
				type = "kloveui.Border",
				group = "Layout",
				description = "Padding around the widget's content.",
			},
			{
				name = "canfocus",
				type = "boolean",
				description = "Whether or not the widget can grab the input focus.",
			},
			{
				name = "focused",
				type = "boolean",
				description = "Whether or not the widget currently has the input focus.",
			},
		},
	},
	{
		classname = "kloveui.Box",
		extends = "kloveui.Widget",
		properties = {
			{
				name = "mode",
				type = "string",
				group = "Common",
				description = "Layout mode."
					.."\n"
					.."\nIf `\"v\"` lays out children vertically. Any other value lays out children"
					.."\nhorizontally.",
			},
			{
				name = "spacing",
				type = "number",
				group = "Layout",
				description = "Spacing between elements.",
			},
		},
	},
	{
		classname = "kloveui.Button",
		extends = "kloveui.Widget",
		properties = {
			{
				name = "text",
				type = "string",
				group = "Common",
				description = "Text label for the button.",
			},
			{
				name = "font",
				type = "love.graphics.Font",
				group = "Text",
				description = "Custom font for the button.",
			},
			{
				name = "texthalign",
				type = "number",
				group = "Text",
				description = "Horizontal alignment for the text.",
			},
			{
				name = "textvalign",
				type = "number",
				group = "Text",
				description = "Vertical alignment for the text.",
			},
			{
				name = "pressed",
				type = "boolean",
				description = "Whether the button is currently pressed.",
			},
		},
		events = {
			{
				name = "activated",
			},
		},
	},
	{
		classname = "kloveui.Check",
		extends = "kloveui.Button",
		properties = {
			{
				name = "value",
				type = "boolean",
				group = "Common",
				description = "Current value of the checkmark.",
			},
		},
	},
	{
		classname = "kloveui.Option",
		extends = "kloveui.Check",
		properties = {
			{
				name = "group",
				type = "string",
				description = "Group of this option.",
			},
		},
	},
	{
		classname = "kloveui.Entry",
		extends = "kloveui.Widget",
		properties = {
			{
				name = "text",
				type = "string",
				group = "Text",
				description = "Text of this entry box.",
			},
			{
				name = "font",
				type = "love.graphics.Font",
				group = "Text",
				description = "Custom font for this entry box.",
			},
			{
				name = "index",
				type = "number",
				description = "Index of insertion caret."
					.."\n"
					.."\nIt refers to the space between characters, where 0 is just before the first"
					.."\ncharacter, 1 is between the first and second characters, and so on."
					.."\n"
					.."\nNote that \"characters\" refers to groups of bytes representing characters in"
					.."\nthe UTF-8 encoding. This is not the same unit Lua uses.",
			},
		},
		events = {
			{
				name = "committed",
			},
		},
	},
	{
		classname = "kloveui.Image",
		extends = "kloveui.Widget",
		properties = {
			{
				name = "image",
				type = "love.graphics.Image",
				group = "Common",
				description = "Image to display.",
			},
			{
				name = "tintcolor",
				type = "kloveui.Color",
				group = "Colors",
				description = "Tint color for the image.",
			},
		},
	},
	{
		classname = "kloveui.Label",
		extends = "kloveui.Widget",
		properties = {
			{
				name = "text",
				type = "string",
				group = "Common",
				description = "Text for the label."
					.."\n"
					.."\nMust be UTF-8 encoded.",
			},
			{
				name = "font",
				type = "love.graphics.Font",
				group = "Text",
				description = "Custom font for this label.",
			},
			{
				name = "texthalign",
				type = "number",
				group = "Text",
				description = "Horizontal alignment for the text.",
			},
			{
				name = "textvalign",
				type = "number",
				group = "Text",
				description = "Vertical alignment for the text.",
			},
		},
	},
	{
		classname = "kloveui.Slider",
		extends = "kloveui.Widget",
		properties = {
			{
				name = "value",
				type = "number",
				group = "Common",
				description = "Current value for the slider."
					.."\n"
					.."\nLies in the range 0-1 (inclusive).",
			},
			{
				name = "increment",
				type = "number",
				group = "Common",
				description = "Value increment."
					.."\n"
					.."\nIf a number, the value snaps to the nearest multiple of that value;"
					.."\nif nil, the value resolution depends on the widget's size.",
			},
			{
				name = "anchor",
				type = "string",
				group = "Common",
				description = "Anchor border."
					.."\n"
					.."\nIf it is `\"l\"`, the value increments left to right."
					.."\nIf it is `\"r\"`, the value increments right to left."
					.."\nIf it is `\"t\"`, the value increments top to bottom."
					.."\nIf it is `\"b\"`, the value increments bottom to top.",
			},
			{
				name = "handlecolorpressed",
				type = "kloveui.Color",
				group = "Colors",
				description = "Color for the handle while pressed.",
			},
		},
		events = {
			{
				name = "valuechanged",
			},
		},
	},
}
