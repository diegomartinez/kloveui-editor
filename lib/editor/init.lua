
---
-- TODO: Doc.
--
-- @module editor

local editor = { }

local widgets = require "editor.data.widgets"
local types = require "editor.data.types"

local Editor = require "editor.ui.editors.Editor"

---
-- TODO: Doc.
--
-- @table Property
-- @tfield string name
-- @tfield string type
-- @tfield string description
-- @tfield string group
-- @tfield any default

for _, w in ipairs(widgets) do
	widgets[w.classname] = w
	w.class = require(w.classname)
	w.properties = w.properties or { }
	for _, p in ipairs(w.properties) do
		p.default = w.class[p.name]
	end
end

for _, t in pairs(types) do
	if t.editorclassname then
		t.editorclass = require(t.editorclassname)
	end
end

local function indexer(parent)
	return setmetatable({ }, { __index = function(self, k)
		local v = parent[k]
		if type(v) == "table" then
			v = indexer(v)
			rawset(self, k, v)
			return v
		end
		return v
	end })
end

---
-- TODO: Doc.
--
-- @tparam kloveui.Widget wid
-- @treturn table Array of `Property`.
function editor.getwidgetprops(wid)
	local w = widgets[wid.__name]
	local t, n = { }, 0
	while w do
		for _, p in ipairs(w.properties or { }) do
			n = n + 1
			p = indexer(p)
			t[n], t[p.name] = p, p
		end
		w = w.extends and widgets[w.extends]
	end
	return t
end

---
-- TODO: Doc.
--
-- @tparam kloveui.Widget wid
-- @treturn table Array of `Event`.
function editor.getwidgetevents(wid)
	local w = widgets[wid.__name]
	local t, n = { }, 0
	while w do
		for _, e in ipairs(w.events or { }) do
			n = n + 1
			e = indexer(e)
			t[n], t[e.name] = e, e
		end
		w = w.extends and widgets[w.extends]
	end
	return t
end

---
-- TODO: Doc.
--
-- @tparam string ptype
-- @treturn editor.ui.Editor
function editor.getpropertyeditor(ptype)
	local e = types[ptype]
	return e and e.editorclass or Editor
end

---
-- TODO: Doc.
--
-- @treturn function Iterator.
function editor.iterwidgets()
	local i = 0
	return function()
		i = i + 1
		return widgets[i], i
	end
end

return editor
