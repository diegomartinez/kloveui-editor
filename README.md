
# KLÖVEUI Editor

A WYSIWYG editor to create user interfaces for [KLÖVEUI][kloveui].

## License

This program is free software released under a permissive license. You can
use it both in Free and closed source software. See `LICENSE.md` for details.

## Features

* As you see, docs are very good.
* All you need is LÖVE.

## API Documentation

API documentation can be generated using using [LDoc][LDoc].

## Requirements

* [LÖVE][love2d] (tested with 11.3, "Mysterious Mysteries").
* [KLÖVEUI][kloveui]
* [klass][klass]
* [kpath][kpath]
* [kglob][kglob]

[love2d]: https://love2d.org
[LDoc]: https://github.com/stevedonovan/LDoc
[kloveui]: https://github.com/kaeza/kloveui
[klass]: https://github.com/kaeza/klass
[kglob]: https://github.com/kaeza/kglob
[kpath]: https://github.com/kaeza/kpath
